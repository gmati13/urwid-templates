class Object:
    reserved = ['set', 'get', 'get_all', 'has']

    def __init__(self, initial_value = None):
        if initial_value is not None:
            if isinstance(initial_value, dict):
                self.properties = initial_value.copy()
            elif isinstance(initial_value, Object):
                self.properties = initial_value.get_all().copy()
            else:
                raise ValueError('Object: "{}" can not be initial value'.format(str(initial_value)))
        else:
            self.properties = {}

    def __getattr__(self, name):
        return self.get(name)

    def set(self, name, value):
        if name in type(self).reserved:
            raise KeyError('Object: can not set property with name "{}"'.format(name))
        self.properties[name] = value

    def get_all(self):
        return self.properties

    def has(self, name):
        return name in self.properties

    def get(self, name):
        if name in self.properties:
            return self.properties[name]
        try:
            return object.__getattribute__(self, name)
        except AttributeError:
            return None
