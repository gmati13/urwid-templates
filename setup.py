import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="urwidtemplates",                     # This is the name of the urwidtemplates
    version="0.0.6",                        # The initial release version
    author="Roman Kovalov",                     # Full name of the author
    description="Library to write TUI interfaces on urwid following angular way",
    long_description=long_description,      # Long description read from the the readme file
    long_description_content_type="text/markdown",
    packages=setuptools.find_packages(),    # List of all python modules to be installed
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],                                      # Information to filter the project on PyPi website
    python_requires='>=3.6',                # Minimum version requirement of the urwidtemplates
    py_modules=["urwidtemplates"],             # Name of the python urwidtemplates
    package_dir={'urwidtemplates':'urwidtemplates'},     # Directory of the source code of the urwidtemplates
    install_requires=[
        'urwid==2.1.2',
        'cssutils~=2.3.0'
    ]                     # Install other dependencies if any
)
