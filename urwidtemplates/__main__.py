from src.UrwidTemplates import UrwidTemplates
import os
import sys

filepath = sys.argv[1]

if filepath is None:
    raise ValueError('filepath not specified')

filepath_absolute = os.path.abspath(os.path.join(os.sep, os.getcwd(), filepath))

app = UrwidTemplates(filepath_absolute)
app.run()
