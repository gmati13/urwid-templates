import urwid
import re

color_list = ['black',
              'dark red',
              'dark green',
              'brown',
              'dark blue',
              'dark magenta',
              'dark cyan',
              'light gray',
              'dark gray',
              'light red',
              'light green',
              'yellow',
              'light blue',
              'light magenta',
              'light cyan',
              'white']


def translate_color(raw_text):
    formated_text = []

    fg = -1
    bg = -1
    decoration = ''

    fgcolor = ''
    bgcolor = ''

    for at in raw_text.split("\x1b["):
        try:
            attr, text = at.split("m", 1)
        except ValueError:
            attr = '0'
            text = at.split("m", 1)

        try:
            list_attr = [int(i) for i in attr.split(';')]
        except ValueError:
            list_attr = []

        use_three = False

        if len(list_attr) == 3:
            use_three = True
            if list_attr[0] == 38:
                fgcolor = 'h' + str(list_attr[2])
            if list_attr[0] == 48:
                bgcolor = 'h' + str(list_attr[2])
            list_attr = []
        else:
            list_attr.sort()
        for elem in list_attr:
            if elem == 0:
                fg = -1
                bg = -1
                decoration = ''
            elif elem == 1 and 'bold' not in decoration:
                decoration += ',bold'
            elif elem == 21:
                decoration = decoration.replace(',bold', '')
            elif elem == 3 and 'italics' not in decoration:
                decoration += ',italics'
            elif elem == 23:
                decoration = decoration.replace(',italics', '')
            elif elem == 4 and 'underline' not in decoration:
                decoration += ',underline'
            elif elem == 24:
                decoration = decoration.replace(',underline', '')
            elif elem == 5 and 'blink' not in decoration:
                decoration += ',blink'
            elif elem == 25:
                decoration = decoration.replace(',blink', '')
            elif elem == 9 and 'strikethrough' not in decoration:
                decoration += ',strikethrough'
            elif elem == 29:
                decoration = decoration.replace(',strikethrough', '')
            elif elem <= 29:
                continue
            elif elem <= 37:
                fg = elem - 30
            elif elem <= 47:
                bg = elem - 40
            elif elem <= 94 or elem >= 100 and elem <= 104:
                continue

        if not use_three:
            fgcolor = color_list[fg]
            bgcolor = color_list[bg]

            if fg < 0:
                fgcolor = ''
            if bg < 0:
                bgcolor = ''

        fgcolor = re.sub(r'^,', '', fgcolor + decoration)

        if len(text) > 0:
            formated_text.append((urwid.AttrSpec(fgcolor, bgcolor), text))

    return formated_text
