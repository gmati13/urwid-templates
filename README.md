# Urwid Templates Framework

Framework based on Urwid to create simple TUI application via simple configuration!

## Usage

[comment]: <> (### Installation)

[comment]: <> (```sh)

[comment]: <> (pip install urwid cssutils)

[comment]: <> (pip install -i https://test.pypi.org/simple/ urwidtemplates)

[comment]: <> (```)

### Example

Files:
```text
main.py
main.yml
```

main.yml
```yaml
widget: Frame
header:
  widget: Text
  markup: header
body:
  widget: ListBox
  body:
    - widget: Text
      markup: body
footer:
  widget: Text
  markup: footer
```

main.py

```python
import os
from urwidtemplates.core import WidgetConfig, yamlreader
import urwid

file = os.path.abspath(
    os.path.join(
        os.sep,
        os.path.dirname(os.path.abspath(__file__)),
        './main.yml'
    )
)
widget_config = WidgetConfig(yamlreader.load(open(file)))

loop = urwid.MainLoop(widget=widget_config.widget)
loop.run()
```

Run
```shell
python main.py
```

[comment]: <> (---)

[comment]: <> (## Contributing)

[comment]: <> (requirements:)

[comment]: <> (```sh)

[comment]: <> (python v3.7)

[comment]: <> (```)

[comment]: <> (### virtualenv configuration:)

[comment]: <> (create virtualenv:)

[comment]: <> (```sh)

[comment]: <> (python3.7 -m venv venv)

[comment]: <> (```)

[comment]: <> (activate virtualenv:)

[comment]: <> (```sh)

[comment]: <> (source venv/bin/activate)

[comment]: <> (```)

[comment]: <> (deactivate virtualenv:)

[comment]: <> (```sh)

[comment]: <> (deactivate)

[comment]: <> (```)

[comment]: <> (### start application)

[comment]: <> (```sh)

[comment]: <> (python main.py)

[comment]: <> (```)

[comment]: <> (### dependencies)

[comment]: <> (install dependencies:)

[comment]: <> (```sh)

[comment]: <> (pip install -r requirements.txt)

[comment]: <> (```)

[comment]: <> (generate or update requirements.txt:)

[comment]: <> (```sh)

[comment]: <> (pip freeze > requirements.txt)

[comment]: <> (```)
