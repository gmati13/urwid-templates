from .BaseLoader import BaseLoader
import re
import importlib.util
import types
import uuid
from ..helpers import relative_path

file_pattern = re.compile(r'^[/\\]?(([A-z0-9-_+]|[.]{1,2})+[/\\])*([A-z0-9]+(\.[A-z0-9]+)*)\.py$')


class FunctionLoader(BaseLoader):
    __cache = {}

    def __call__(self, path_or_code, scope = None):
        if file_pattern.match(path_or_code):
            abspath = relative_path(scope.path, path_or_code)
            return self.get_from_cache_or_create(abspath)
        pymodule = types.ModuleType(str(uuid.uuid4()))
        exec(path_or_code, pymodule.__dict__)
        return pymodule.handler

    def get_from_cache_or_create(self, path):
        if path in type(self).__cache:
            return type(self).__cache[path]
        spec = importlib.util.spec_from_file_location('module.name', path)
        pymodule = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(pymodule)
        type(self).__cache[path] = pymodule.handler
        return pymodule.handler
