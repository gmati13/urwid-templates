import urwid
from .types import Object
from .loaders import ModuleLoader
from .EventLoop import EventLoop

class UrwidTemplates:
    def __init__(self, module_abspath):
        loader = ModuleLoader()
        self.root_module = loader(module_abspath)
        self.event_loop = EventLoop()
        self.event_loop.start()

    def render(self, config: Object = None):
        if config is None:
            return self.render(self.root_module.widget)

        if isinstance(config, list):
            return [self.render(w) for w in config]

        if not isinstance(config, Object):
            return config

        creator = object.__getattribute__(urwid, config.type)
        arguments = \
            { k: self.render(config.arguments.get(k)) for k in config.arguments.get_all() } \
            if config.arguments else {}

        module = config.scope.module
        widget = creator(**arguments)

        if module.store and not callable(module.store.emit):
            module.store.set_emitter(EventLoop.create_emitter(module, widget))
        if module.scope.global_store and not callable(module.scope.global_store.emit):
            module.scope.global_store.set_emitter(EventLoop.create_emitter(module, widget))

        if config.events:
            for event in config.events.get_all():
                reactions = config.events.get(event)

                if isinstance(reactions, str):
                    EventLoop.subscribe(module, widget, event, reactions)
                else:
                    for reaction in reactions:
                        EventLoop.subscribe(module, widget, event, reaction)
                if event.startswith('Widget:') and event != 'Widget:Init':
                    EventLoop.override(module, widget, event[7:])

            if 'Widget:Init' in config.events.get_all():
                scope = {
                    'emit': EventLoop.create_emitter(module, widget),
                    'widget': widget,
                    'store': module.store,
                    'global_store': module.scope.global_store
                }
                instructions = config.events.get('Widget:Init')
                if isinstance(instructions, str):
                    exec(instructions, scope)
                else:
                    for instruction in instructions:
                        exec(instruction, scope)

        return widget

    def run(self):
        app = urwid.MainLoop(self.render(), screen=urwid.raw_display.Screen())
        app.screen.set_terminal_properties(colors=256)
        self.event_loop.on_task_complete(app.draw_screen)
        app.run()
