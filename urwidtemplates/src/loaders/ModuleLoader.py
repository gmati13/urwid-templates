from .BaseLoader import BaseLoader
from .FunctionLoader import FunctionLoader
from .WidgetLoader import WidgetLoader
from .StoreLoader import StoreLoader
from ..types import Object
from ..helpers import yamlreader, relative_path


class ModuleLoader(BaseLoader):
    __cache = {}

    def __call__(self, path_or_config, scope = None):
        if isinstance(path_or_config, str):
            if scope is None:
                return self.get_from_cache_or_create(path_or_config, scope)
            return self.get_from_cache_or_create(relative_path(scope.path, path_or_config), scope)
        return BaseLoader.__call__(self, path_or_config, scope)

    def get_from_cache_or_create(self, path, scope = None):
        if path in ModuleLoader.__cache:
            return ModuleLoader.__cache[path]
        config = yamlreader.load(open(path))
        if scope and scope.global_store:
            result = BaseLoader.__call__(self, config, Object({ 'path': path, 'global_store': scope.global_store }))
        else:
            result = BaseLoader.__call__(self, config, Object({ 'path': path }))
        type(self).__cache[path] = result
        return result

    def create_store(self, scope = None):
        if scope is None:
            scope = Object()
        store = type(self).store_creator()
        scope.set('module', store)
        store.set('scope', scope)
        return store

ModuleLoader.markup = {
    'GlobalStore': ('set', 'scope.global_store', StoreLoader('GlobalStore')),
    'Modules':     ('reduce', 'modules', ModuleLoader()),
    'Functions':   ('reduce', 'functions', FunctionLoader()),
    'Widget':      ('set', 'widget', WidgetLoader()),
    'Store':       ('set', 'store', StoreLoader()),
}
