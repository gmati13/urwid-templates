from .BaseLoader import BaseLoader
from ..types import Object

class Store(Object):
    reserved = Object.reserved + ['set_emitter', 'set_name']

    def __init__(self, *args, **kwargs):
        super(Store, self).__init__(*args, **kwargs)
        self.emit = None
        self.name = 'Store'

    def set(self, name, value):
        super(Store, self).set(name, value)
        if callable(self.emit):
            self.emit('{}:{}'.format(self.name, name), value)

    def set_emitter(self, emitter):
        self.emit = emitter

    def set_name(self, name):
        self.name = name


class StoreLoader(BaseLoader):
    def __init__(self, name = None):
        super(StoreLoader, self).__init__()
        self.name = name

    def __call__(self, schema, scope = None):
        store = self.create_store(scope)
        if self.name:
            store.set_name(self.name)
        for key in schema:
            store.set(key, schema[key])
        return store


StoreLoader.store_creator = Store
