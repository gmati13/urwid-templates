import os

def relative_path(base, relative):
    return os.path.abspath(os.path.join(os.sep, os.path.dirname(base), relative))
