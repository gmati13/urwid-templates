from threading import Thread
from queue import Queue


class EventLoop:
    queue = Queue()
    subscribers = {}
    pattern = '{}@{}'

    def __init__(self):
        self.thread = Thread(target=self.__worker, daemon=True)
        self.complete = lambda: None

    def on_task_complete(self, complete):
        self.complete = complete

    def __worker(self):
        while True:
            source, event, payload = self.queue.get()
            key = EventLoop.pattern.format(source, event)
            if key in EventLoop.subscribers:
                for scope, reaction in EventLoop.subscribers[key]:
                    exec(reaction, { **scope, 'payload': payload })
            self.queue.task_done()
            self.complete()

    def start(self):
        self.thread.start()

    @staticmethod
    def subscribe(module, widget, event, reaction):
        scope = {
            'emit': EventLoop.create_emitter(module, widget),
            'widget': widget,
            'store': module.store,
            'global_store': module.scope.global_store
        }
        if module.functions:
            scope = { **scope, ** module.functions.get_all() }
        if event.startswith('Global:') or event.startswith('GlobalStore:'):
            key = EventLoop.pattern.format(0, event)
        elif event.startswith('Widget:'):
            key = EventLoop.pattern.format(str(id(widget)), event)
        else:
            key = EventLoop.pattern.format(str(id(module)), event)
        if key not in EventLoop.subscribers:
            EventLoop.subscribers[key] = []
        EventLoop.subscribers[key].append((scope, reaction))

    @staticmethod
    def create_emitter(module, widget):
        module_id = id(module)
        widget_id = id(widget)

        def make_event(event_name: str, payload):
            nonlocal module_id, widget_id
            if event_name.startswith('Global:') or event_name.startswith('GlobalStore:'):
                return 0, event_name, payload
            if event_name.startswith('Widget:'):
                return widget_id, event_name, payload
            return module_id, event_name, payload

        return lambda e, p: EventLoop.queue.put(make_event(e, p))

    @staticmethod
    def override(module, widget, function):
        emitter = EventLoop.create_emitter(module, widget)
        original = getattr(widget, function)

        def wrapper(*args, **kwargs):
            result = original(*args, **kwargs)
            emitter('Widget:' + function, (*args, *kwargs))
            return result

        setattr(widget, function, wrapper)
