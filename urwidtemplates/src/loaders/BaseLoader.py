from ..types import Object

class BaseLoader(Object):
    markup = {}
    store_creator = Object

    def __init__(self, markup = None):
        super(Object, self).__init__()
        if markup is not None:
            type(self).markup = markup

    def __call__(self, schema, scope = None):
        store = self.create_store(scope)
        for key in type(self).markup:
            if key in schema:
                type(self).load_markup(store, *type(self).markup[key], schema, schema[key])
        return store

    def create_store(self, scope = None):
        if scope is not None:
            return type(self).store_creator({ 'scope': Object(scope) })
        return type(self).store_creator()

    @staticmethod
    def load_markup(store, rule_or_switcher, target_or_markup, loader_or_markup, schema, subtree):
        if callable(rule_or_switcher):
            if rule_or_switcher(schema, subtree):
                BaseLoader.load_markup(store, *target_or_markup, schema, subtree)
            else:
                BaseLoader.load_markup(store, *loader_or_markup, schema, subtree)
        else:
            target, direction = BaseLoader.find_target_and_direction(store, target_or_markup)
            if rule_or_switcher == 'set':
                target.set(direction, loader_or_markup(subtree, store.scope))
            elif rule_or_switcher == 'map':
                array = [loader_or_markup(child, store.scope) for child in subtree]
                target.set(direction, array)
            elif rule_or_switcher == 'reduce':
                obj = BaseLoader.define_and_get_target_if_not_exists(target, direction)
                for key in subtree:
                    obj.set(key, loader_or_markup(subtree[key], store.scope))

    @staticmethod
    def find_target_and_direction(store, path_string):
        path = path_string.split('.')
        if len(path) == 1:
            return store, path_string

        target = store
        while len(path) > 1:
            target = BaseLoader.define_and_get_target_if_not_exists(target, path[0])
            path = path[1:]

        return target, path[0]

    @staticmethod
    def define_and_get_target_if_not_exists(target, name):
        if isinstance(target, Object):
            if not target.has(name):
                target.set(name, Object())
            return target.get(name)
        else:
            if not name in target:
                target[name] = Object()
            return target[name]
