from .BaseLoader import BaseLoader

class ValueLoader(BaseLoader):
    modifiers = []

    def __init__(self, value_type = None):
        super(BaseLoader, self).__init__()
        self.type = value_type

    def __call__(self, value, *argv):
        if self.type in type(self).modifiers:
            return type(self).modifiers[self.type](value)
        return value

ValueLoader.modifiers = {
    'number': lambda v: int(v),
    'string': lambda v: str(v)
}
