def handler(widget, key, emit):
    if key == 'enter':
        emit('Global:SetText', widget.get_edit_text())
        widget.set_edit_text('')
