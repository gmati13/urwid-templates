import yaml

__loader = yaml.BaseLoader


def load(stream):
    return yaml.load(stream, Loader=__loader)
