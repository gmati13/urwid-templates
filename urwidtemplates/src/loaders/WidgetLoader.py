from .BaseLoader import BaseLoader
from .ValueLoader import ValueLoader


class WidgetLoader(BaseLoader):
    def __call__(self, module_or_config, scope = None):
        if isinstance(module_or_config, str) and scope is None:
            raise ValueError('WidgetLoader: cannot load widget from module without scope')
        if isinstance(module_or_config, str) and scope is not None:
            return WidgetLoader.store_creator(scope.module.modules.get(module_or_config).widget)
        return BaseLoader.__call__(self, module_or_config, scope)


WidgetLoader.markup = {
    'Body': (
        lambda schema, _: schema['Type'] in ['ListBox'],
        ('map', 'arguments.body', WidgetLoader()),
        ('set', 'arguments.body', WidgetLoader())
    ),
    'WidgetList': ('map', 'arguments.widget_list', WidgetLoader()),
    'BoxWidget':  ('set', 'arguments.box_widget', WidgetLoader()),
    'Footer':     ('set', 'arguments.footer', WidgetLoader()),
    'Header':     ('set', 'arguments.header', WidgetLoader()),
    'Height':     ('set', 'arguments.height', ValueLoader('number')),
    'Markup':     ('set', 'arguments.markup', ValueLoader('string')),
    'Caption':    ('set', 'arguments.caption', ValueLoader('string')),
    'EditText':   ('set', 'arguments.edit_text', ValueLoader('string')),
    'Events':     ('reduce', 'events', ValueLoader()),
    'Type':       ('set', 'type', ValueLoader('string'))
}
